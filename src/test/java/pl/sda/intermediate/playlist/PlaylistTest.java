package pl.sda.intermediate.playlist;

import org.junit.jupiter.api.Test;


public class PlaylistTest {
    @Test
    public void sequanceAndRandomInside(){
        //given
        Playlist playlist = new Playlist();
        Playlist innerPlaylist = new Playlist();
        Music song1 = new Music("Song1", "Author1");
        Movie movie1 = new Movie("Titanic");
        Music song2 = new Music("Song2","Author2");
        Movie movie2 = new Movie("Star");
        playlist.addToPlaylist(song1);
        playlist.addToPlaylist(movie1);
        innerPlaylist.addToPlaylist(song2);
        innerPlaylist.addToPlaylist(movie2);
        playlist.addToPlaylist(innerPlaylist);
        playlist.choosePlayType(PlayType.SEQUENCE);
        innerPlaylist.choosePlayType(PlayType.RANDOM);
        //when
        System.out.println(playlist.play());

        //then
    }

}