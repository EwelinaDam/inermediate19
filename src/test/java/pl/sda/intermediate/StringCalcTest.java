package pl.sda.intermediate;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pl.sda.intermediate.other.NegativeNumberExeption;
import pl.sda.intermediate.other.StringCalculator;

public class StringCalcTest {
    @Test
    public void shouldReturnZeroWhenStringIsEmpty() {
        //given
        String string = "";
        //when
        int result = StringCalculator.adding(string);

        //then
        Assertions.assertEquals(0,result);
    }

    @Test
    public void shouldReturnSumWhenGivenOneNumber() {
        //given
        String string = "1";
        //when
        int result = StringCalculator.adding(string);

        //then
        Assertions.assertEquals(1, result);
    }

    @Test
    public void shouldReturnSumWhenGivenTwoNumbers() {
        //given
        String string = "1 ,2 ";
        //when
        int result = StringCalculator.adding(string);

        //then
        Assertions.assertEquals(3, result);
    }
    @Test
    public void shouldReturnSumWhenGivenManyNumbers() {
        //given
        String string = "1 ,2,5,    8 ";
        //when
        int result = StringCalculator.adding(string);

        //then
        Assertions.assertEquals(16, result);
    }
    @Test
    public void shouldReturnSumWhenGivenManyNumbersSeparatedWithComaAndNewLine() {
        //given
        String string = "1 ,2 \n 3";
        //when
        int result = StringCalculator.adding(string);

        //then
        Assertions.assertEquals(6, result);
    }

    @Test
    public void shouldReturnSumWhenGivenManyNumbersSeparatedWithCastomDelimiter() {
        //given
        String string = "//;\n1;2;3";
        //when
        int result = StringCalculator.adding(string);

        //then
        Assertions.assertEquals(6, result);
    }

    @Test
    public void shouldReturnSumWhenGivenManyNumbersSeparatedWithNewLineDelimiter() {
        //given
        String string = "//\n\n1\n2\n3";
        //when
        int result = StringCalculator.adding(string);

        //then
        Assertions.assertEquals(6, result);
    }
    @Test
    public void shouldThrowExeptionWhenGivenNegativeNumbers() {
        //given
        String string = "//&\n-1&2&-3";

        NegativeNumberExeption exeption = Assertions.assertThrows(NegativeNumberExeption.class,
                () -> StringCalculator.adding(string));
        Assertions.assertEquals("Negative numbers not allowed: -1, -3", exeption.getMessage());
    }

    @Test
    public void shouldReturnSumAndIgnoreNumberBiggerThenOneThousend() {
        //given
        String string = "//>\n1000>1001>3";
        //when
        int result = StringCalculator.adding(string);

        //then
        Assertions.assertEquals(1003, result);
    }
    @Test
    public void shouldReturnSumWhenGivenManyNumbersSeparatedWithLongCastomDelimiter() {
        //given
        String string = "//[&&&]\n1 &&&2&&&  3";
        //when
        int result = StringCalculator.adding(string);

        //then
        Assertions.assertEquals(6, result);
    }


}
