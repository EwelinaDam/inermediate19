package pl.sda.intermediate;

import org.junit.jupiter.api.Test;
import pl.sda.intermediate.other.CarConstantHashCode;
import pl.sda.intermediate.other.CarRandomHashcode;

import java.util.HashSet;
import java.util.Set;

public class CarHashCodeTest {


        @Test
        public void hashCodeTest() {
            CarConstantHashCode const1 = new CarConstantHashCode("Fiat");
            CarConstantHashCode const2 = new CarConstantHashCode("Opel");
            CarConstantHashCode const3 = new CarConstantHashCode("Ferrari");
            CarConstantHashCode const4 = new CarConstantHashCode("Nissan");
            CarConstantHashCode const5 = new CarConstantHashCode("Skoda");
            CarConstantHashCode const6 = new CarConstantHashCode("Dacia");

            Set<CarConstantHashCode> set = new HashSet<>();
            set.add(const1);
            set.add(const2);
            set.add(const3);
            set.add(const4);
            set.add(const5);
            set.add(const6);

            System.out.println(set.size());

        }
        @Test
        public void randomHashCodeTest() {
            CarRandomHashcode const1 = new CarRandomHashcode("Fiat");
            CarRandomHashcode const2 = new CarRandomHashcode("Opel");
            CarRandomHashcode const3 = new CarRandomHashcode("Ferrari");
            CarRandomHashcode const4 = new CarRandomHashcode("Nissan");
            CarRandomHashcode const5 = new CarRandomHashcode("Skoda");
            CarRandomHashcode const6 = new CarRandomHashcode("Dacia");

            Set<CarRandomHashcode> set = new HashSet<>();
            set.add(const1);
            set.add(const2);
            set.add(const3);
            set.add(const4);
            set.add(const5);
            set.add(const6);

            System.out.println(set.size());

        }


    }


