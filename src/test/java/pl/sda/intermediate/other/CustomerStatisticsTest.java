package pl.sda.intermediate.other;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.util.Assert;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CustomerStatisticsTest {
    @Test
    void shouldReturnMapSortedBySalaryForGivenArrayOfCutomers() {
        System.out.println(CustomerStatistics.groupBySalary());
    }

    @Test
    void usingStreamShouldReturnMapSortedBySalaryForGivenArrayOfCutomersme() {
        System.out.println(CustomerStatistics.groupBySalaryStream());
    }

    @Test
    void shouldReturnBigDecimalWhenGivenSth() {
        //given
        String salary1 = "2";
        String salary2 = "5 ";
        String salaryNull = null;

        System.out.println(new BigDecimal(salary1));
        System.out.println(new BigDecimal(salary2.trim()));
//        System.out.println(new BigDecimal(salaryNull)); // tu jest problem

    }

    @Test
    void shouldReturnUniqueNamesInDecendingOrder() {
        //when
        List<String> uniqueNames = CustomerStatistics.uniqueNames();
        System.out.println(uniqueNames);

        //then
        List<String> expected = new ArrayList<>();
        expected.add("Monika");
        expected.add("Marek");
        expected.add("Beata");
        expected.add("Anna");
        expected.add("Adam");
        System.out.println(expected);

        Assertions.assertEquals(uniqueNames, expected);
    }
}
