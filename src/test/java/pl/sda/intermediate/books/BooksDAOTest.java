package pl.sda.intermediate.books;

import org.junit.jupiter.api.Test;
import org.junit.platform.commons.util.StringUtils;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BooksDAOTest {
    @Test
    void sublistTest() {
        String book1 = "The German Fleet, by Archibald Hurd                                      56653\n" +
                " [Subtitle: Being The Companion Volume to \"The Fleets At War\"\n" +
                "  and \"From Heligoland To Keeling Island]";
        String book2 = "Primus Annus, by Walter Lionel Paine and Cyril Lyttleton Mainwaring      56651\n";
        String book3 = "Kuvaelmia itä-suomalaisten vanhoista tavoista 5: Kesäaskareet,           56115\n" +
                " by Johannes Häyhä\n" +
                " [Language: Finnish]";

        List<String> list = new ArrayList<>();

        list.add(book1);
        list.add(book2);
        list.add(book3);

        for (String book : list) {
            String[] bookInfo = book.split("\\s+\\d+\\n");
            System.out.println(Arrays.stream(bookInfo).collect(Collectors.joining()));
            System.out.println(book.replaceAll("[^(\\d+\\n)]", "").trim());
        }
    }

}

