package pl.sda.intermediate;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class LambdaTest {

    private List<String> animals = Arrays.asList("Rat", "Cat", "Dog", " ", "Mouse", null);

    @Test
    void testingLambdaOne() {
        Comparator<String> thirdletterComparator =
                (a, b) -> String.valueOf(a.charAt(2)).compareTo(String.valueOf(b.charAt(2)));
        Comparator<String> naturalFirstLetterComparator =
                (a, b) -> a.compareTo(b);
        Comparator<String> firstLetterCompAnonymous = new Comparator<String>() {
            @Override
            public int compare(String s1, String s2) {
                return s1.compareTo(s2);
            }
        };

        Predicate<String> notBlankPredicate = a -> StringUtils.isNotBlank(a);

        System.out.println(animals.stream()
                .filter(notBlankPredicate)
                .sorted(thirdletterComparator.thenComparing(naturalFirstLetterComparator))
                .collect(Collectors.toList()));
    }

    @FunctionalInterface
    public interface SuperChecker<T> {
        boolean check(T someValue); //jedyna metoda abstrakcyjna w funkcionalInterface

        default SuperChecker<T> reversedValue() {
            return previousValue -> !this.check(previousValue);
        }
    }

    @Test
    void lambdaTestTwo() {
        SuperChecker<Integer> checkIfOdd = a -> a % 2 != 0;
        System.out.println(checkIfOdd.check(3));
        System.out.println(checkIfOdd.reversedValue().check(3));

    }

    @FunctionalInterface
    public interface SuperCalculator<T, E> {
        int calculate(T firstValue, E secondValue);
    }

    @Test
    void lambdaTestThree() {
        SuperCalculator<Integer, String> adder =
                (a, b) -> a + Integer.parseInt(b);

        System.out.println(adder.calculate(2, "3"));

        BiFunction<Integer, String, Integer> biAdder =
                (a, b) -> a + Integer.parseInt(b);

        System.out.println(adder.calculate(3 , "4"));

    }


}
