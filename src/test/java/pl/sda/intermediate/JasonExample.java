package pl.sda.intermediate;

import com.google.gson.Gson;
import lombok.Getter;
import lombok.Setter;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.URL;
import java.net.URLConnection;
import java.util.*;

public class JasonExample {

    @Test
    void serializedJson() {
        SomeObject someObject = new SomeObject();
        someObject.setName("Adam");
        someObject.setAge(25);
        someObject.setSalary(BigDecimal.valueOf(3000.2));
        List<OtherObject> otherObjects = new ArrayList<>();
        OtherObject otherObject = new OtherObject();
        otherObject.setId(1);
        otherObject.setText("Text");
        otherObjects.add(otherObject);
        OtherObject otherObject2 = new OtherObject();
        otherObject2.setId(2);
        otherObject2.setText("Text2");
        otherObjects.add(otherObject2);
        someObject.setOtherObject(otherObject);

        Map<Integer, String> newMap = new HashMap<>();
        newMap.put(1, "sth");
        newMap.put(2, "sth");
        someObject.setMap(newMap);

        String json = new Gson().toJson(someObject);
        System.out.println(json);

        SomeObject objectFromJson = new Gson().fromJson(json, SomeObject.class);
    }

    @Setter
    private class SomeObject {
        private String name;
        private int age;
        private BigDecimal salary;
        private List<OtherObject> list;
        private Map<Integer, String> map;
        private boolean isItTrue;
        private OtherObject otherObject;
    }

    @Setter
    private class OtherObject {
        private int id;
        private String text;
    }

    @Test
    void nbpTest() throws Exception {
        URL url = new URL("http://api.nbp.pl/api/exchangerates/tables/A/last?format=json");
        URLConnection urlConnection = url.openConnection();
        InputStream inputStream = urlConnection.getInputStream();
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
        BufferedReader br = new BufferedReader(inputStreamReader);

        String reuslt = "";
        String inputLine;
        while ((inputLine = br.readLine()) != null) {
            reuslt = reuslt + inputLine;
        }
        System.out.println(reuslt);

        RatesWrapper[] rw = new Gson().fromJson(reuslt, RatesWrapper[].class);
        System.out.println(Arrays.toString(rw));

    }

    @Getter
    @Setter
    private class RatesWrapper {
        private String table;
        private String no;
        private String effectiveDate;
        private List<Rate> rates;
    }

    @Getter
    @Setter
    private class Rate {
        private String currency;
        private String code;
        private Double mid;
    }

}