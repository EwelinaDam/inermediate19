package pl.sda.intermediate;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class BankTest {
    @Test
    public void synchronizedBankTest() {
        for (int i = 0; i < 100; i++) {
            ClientAction client = new ClientAction();
            client.run();
        }

    }

    @Test
    public void threadsBankTest() {
        List<Thread> list = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            ClientAction clientAction = new ClientAction();
            Thread thread = new Thread(clientAction); //to jest pracownik
            list.add(thread);
        }
        for (Thread thread : list) {
            thread.start(); //uruchomi pracę w danym wątku, a nie w mainie
        }
        for (Thread thread : list) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Stan konta " + Bank.getAccount());
        System.out.println("Licznik operacji " + Bank.getCounter());
    }
}
