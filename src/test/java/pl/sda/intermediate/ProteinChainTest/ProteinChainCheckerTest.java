package pl.sda.intermediate.ProteinChainTest;

import org.junit.jupiter.api.Test;
import org.junit.platform.commons.util.StringUtils;
import pl.sda.intermediate.laboratory.ProteinChainChecker;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ProteinChainCheckerTest {
    @Test
    void operationsOnString() {
        String chain1 = "AFGBCDN";
        String chain2 = "GFABDNC";
        String chain3 = "AAAAAAA";

        System.out.println(chain1.length());
        String sorted1 = Arrays.stream(chain1.split("")).sorted().collect(Collectors.joining());
        String sorted2 = Arrays.stream(chain2.split("")).sorted().collect(Collectors.joining());
        String sorted3 = Arrays.stream(chain3.split("")).sorted().collect(Collectors.joining());

        System.out.println(sorted1.equals(sorted2));
        System.out.println(sorted1.equals(sorted3));

    }

    @Test
    void shouldReturnListOfAnswers() {
        //given
        ProteinChainChecker proteinChainChecker = new ProteinChainChecker();

        //when
        List<Boolean> answers = proteinChainChecker.getAnswer();
        System.out.println(answers);
    }
}
