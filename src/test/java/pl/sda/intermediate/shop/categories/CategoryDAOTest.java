package pl.sda.intermediate.shop.categories;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class CategoryDAOTest {
    @Test
    public void shouldPopulateProperly() {
        //given
        Category testCategory = new Category();
        testCategory.setParentId(4);
        testCategory.setId(6);
        testCategory.setLevel(2);
        testCategory.setCategName("Klasa druga");
        //when
        CategoryDAO categoryDAO = CategoryDAO.getInstance();
        Category findCategory = categoryDAO.getCategoryList()
                .stream()
                .filter(c -> c.getId().equals(6))
                .findFirst().orElse(null);
        //then
        Assertions.assertEquals(testCategory.getLevel(),findCategory.getLevel());
        Assertions.assertEquals(testCategory.getId(),findCategory.getId());
        Assertions.assertEquals(testCategory.getParentId(),findCategory.getParentId());
        Assertions.assertEquals(testCategory.getCategName(),findCategory.getCategName());

    }

}