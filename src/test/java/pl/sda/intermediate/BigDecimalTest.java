package pl.sda.intermediate;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

public class BigDecimalTest {

    @Test
    public void bDT (){
        double x = 0.02;
        double y = 0.03;
        System.out.println(x - y);

        BigDecimal a = new BigDecimal(0.02);
        BigDecimal b = new BigDecimal(0.03);
        System.out.println(b.subtract(a));

        BigDecimal c = BigDecimal.valueOf(0.02);
        BigDecimal d = BigDecimal.valueOf(0.03);

        System.out.println(d.subtract(c));


    }
}
