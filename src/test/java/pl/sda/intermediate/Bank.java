package pl.sda.intermediate;

import lombok.Getter;

import java.math.BigDecimal;
import java.util.concurrent.atomic.AtomicInteger;

public class Bank {
//    private static.css Integer account = 1000;
    private static AtomicInteger account = new AtomicInteger(1000);
    private static Integer counter = 0;

    public static Integer getAccount() {
        return account.get();
    }

    public static Integer getCounter() {
        return counter;
    }

    public static /*synchronized*/ void withdrow(Integer howMuch) {
        account.addAndGet(-howMuch);
        System.out.println(Thread.currentThread().getName()
                + " Withdraw: " + account);
    }

    public static /*synchronized*/ void deposit(Integer howMuch) {
        account.addAndGet(howMuch);
        System.out.println(Thread.currentThread().getName()
                + " Deposit: " + account);
        counter++;
    }
}
