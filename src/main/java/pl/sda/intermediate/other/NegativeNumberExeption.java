package pl.sda.intermediate.other;

import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class NegativeNumberExeption extends RuntimeException {
    public NegativeNumberExeption(List<Integer> negatives) {
        super("Negative numbers not allowed: " + negatives.stream()
                .map(num -> String.valueOf(num))
                .collect(Collectors.joining(", ")));
    }
}
