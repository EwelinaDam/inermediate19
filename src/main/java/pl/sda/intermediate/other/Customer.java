package pl.sda.intermediate.other;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class Customer {
    private static int counter = 1;

    //@Setter(AccessLevel.NONE) //brak Settera dla id
    private Integer id;
    private String name;
    private String lastName;
    private Integer age;
    private BigDecimal salary;

    // przy utworzeniu nowego obiektu najpierw wykona się blok inicjalizujący, a najpierw rzeczy statyczne
    {
        id = counter++;
    }

    public Customer(String name, String lastName, Integer age, String salary) {
        this.name = name.trim();
        this.lastName = lastName.trim();
        this.age = age;
        if (salary == null) {
            this.salary = null;
        } else {
            this.salary = new BigDecimal(salary.trim());
        }
//        id = counter++; zamienione przez blok inicjalizacyjny
    }

    public Customer(String name, String lastName, Integer age, int salary) {
        this(name, lastName, age, String.valueOf(salary));
//        this.name = name;
//        this.lastName = lastName;
//        this.age = age;
//        this.salary = BigDecimal.valueOf(salary);
    }

    @Override
    public String toString() {
        return String.format("id: %s, name: %s, lastname: %s, age: %s",
                id, name, lastName, age);
    }
}
