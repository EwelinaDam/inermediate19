package pl.sda.intermediate.other;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Objects;

@Getter
@AllArgsConstructor
public class CarConstantHashCode {
    String model;

    @Override
    public int hashCode() {
        return 5;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        CarConstantHashCode that = (CarConstantHashCode) obj;
        return Objects.equals(model, that.model);
    }
}
