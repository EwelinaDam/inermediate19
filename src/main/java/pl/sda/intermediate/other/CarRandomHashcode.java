package pl.sda.intermediate.other;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Objects;
import java.util.Random;

@Getter
@AllArgsConstructor
public class CarRandomHashcode {
    String model;

    @Override
    public int hashCode() {
        Random random = new Random();
        return random.nextInt();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        CarConstantHashCode that = (CarConstantHashCode) obj;
        return Objects.equals(model, that.model);
    }
}
