package pl.sda.intermediate.other;


import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

public class CustomerStatistics {
    private static Customer[] people = new Customer[]{
            new Customer("Anna", "Nowak   ", 33, "1200"),
            new Customer("Beata", "Kowalska", 22, "1200"),
            new Customer("Marek", " Nowak", 25, "1250 "),
            new Customer("Adam", "Twardowski", 33, "4100 "),
            new Customer("Monika  ", "Kos", 25, "2500"),
            new Customer("Adam ", "Rudy", 45, "3333"),
            new Customer("Marek", "Rudy", 15, 2210),
            new Customer("Adam", "Madej", 15, 3333),
            new Customer("Adam", "Nowak ", 15, 5700)
    };

    private static CarOption[] items = new CarOption[]{
            new CarOption("Klima",1500),
            new CarOption("Radyjko",1200),
            new CarOption("Wycieraczki",100),
            new CarOption("Dywaniki",150)
    };

    /**
     * Napisz metodę, która
     * zamieni tabtlicę people na listę people - ta metoda może
     * być potem wykorzystywana przez Was w następnych metodach
     */
    public static List<Customer> createList() {
        return Arrays.asList(people); //ta lista to nie jest TA THE Arraylist, to tutaj jest immutable
    }

    /**
     * . Napisz metodę,
     * która przyjmie tablicę people i zwróci listę
     * Stringów <imię nazwisko> (elementem listy będzie np "Anna Nowak")
     */

    public static List<String> namesList() {
        ArrayList<String> list = new ArrayList<String>();
        for (Customer person : people) {
            list.add(person.getName() + " " + person.getLastName());
        }
        return list;
    }

    public static List<String> namesListwithStream() {
        return Arrays.stream(people)
                .map(c -> c.getName() + " " + c.getLastName())
                .collect(Collectors.toList());
    }


    // Napisz metodę, która zwróci mapę osób <id,osoba>
    public static Map<Integer, Customer> mapOfPerson() {
        Map<Integer, Customer> mapOfPeople = new HashMap<Integer, Customer>();
        for (Customer person : people) {
            mapOfPeople.put(person.getId(), person);
        }
        return mapOfPeople;
    }

    public static Map<Integer, Customer> mapIdPersonStream() {
        return Arrays.stream(people).collect(Collectors.toMap(p -> p.getId(), p -> p));
    }

    //Napisz metodę, która zwróci mapę osób według zarobków <zarobki,osoby_z_zarobkami>
    public static Map<BigDecimal, List<Customer>> groupBySalary() {
        HashMap<BigDecimal, List<Customer>> mapBySalary = new HashMap<>();

        for (Customer person : people) {
            BigDecimal salary = person.getSalary();
            if (mapBySalary.containsKey(salary)) {
                mapBySalary.get(salary).add(person);
            } else {
                List<Customer> list = new ArrayList<>();
                list.add(person);
                mapBySalary.put(salary, list);
            }
        }
        return mapBySalary;
    }

    /**
     * matoda ze streamem nie zadziała, jeśli w danych wejściowych
     * salary będzie miało wartość null
     * collector nie chce mapować do wartości nullowych
     *
     * @return
     */
    public static Map<BigDecimal, List<Customer>> groupBySalaryStream() {
        return Arrays.stream(people).collect(Collectors.groupingBy(p -> p.getSalary()));
    }

    //Napisz metodę, która zwróci mapę map <imię,<zarobki, liczba_osób_z_takimi_zarobkami

    public static Map<String, Map<BigDecimal, Integer>> salaryStatisticsByName() {
        Map<String, Map<BigDecimal, Integer>> mapByName = new HashMap<>();
        for (Customer person : people) {
            String name = person.getName();
            if (mapByName.containsKey(name)) {
                Map<BigDecimal, Integer> innerMap
                        = mapByName.get(name);
                if (innerMap.containsKey(person.getSalary())) {
                    Integer count = innerMap.get(person.getSalary());
                    innerMap.put(person.getSalary(), count + 1);
                } else {
                    innerMap.put(person.getSalary(), 1);
                }
            } else {
                Map<BigDecimal, Integer> innerMap
                        = new HashMap<>();
                innerMap.put(person.getSalary(), 1);
                mapByName.put(person.getName(), innerMap);
            }
        }
        return mapByName;
    }

    public static Map<String, Map<BigDecimal, Long>> salaryStatisticsByNameStream() {
        return Arrays.stream(people)
                .collect(Collectors.groupingBy(p -> p.getName(),
                        Collectors.groupingBy(p -> p.getSalary(),
                                Collectors.counting())));
    }

    /**
     * Napisz metodę, która zwróci kolekcję unikalnych imion posortowanych alfabetycznie, ale malejącej kolejności
     */
    public static List<String> uniqueNames() {
        return Arrays.stream(people).
                map(Customer::getName).
                sorted((s, anotherString) -> -s.compareTo(anotherString)).
                distinct().
                collect(Collectors.toList());

    }


}
