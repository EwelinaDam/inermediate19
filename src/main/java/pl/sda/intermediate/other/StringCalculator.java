package pl.sda.intermediate.other;

import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class StringCalculator {
    public static int adding(String string) {
//        if (string.equals("")) return 0;
//        if (StringUtils.isBlank(string) && StringUtils.isEmpty(string)) {
//            return 0;
//        }
        String trimmedString = string.trim();
        String regex = "[,\n]";
        if (trimmedString.startsWith("//")) {
            Pattern pattern = Pattern.compile("(//)(.+)(\n)(.+)");
            Matcher matcher = pattern.matcher(trimmedString);
            matcher.matches();
            regex = matcher.group(2).replaceAll("[\\[\\]]","");
            trimmedString = matcher.group(4);

//            regex = String.valueOf(trimmedString.charAt(2));
//            String[] data = trimmedString.split(regex);
//            trimmedString = Arrays.stream(data).skip(1).collect(Collectors.joining(regex));
        }
        List<Integer> numbers = Arrays.stream(trimmedString.split(regex))
                .map(String::trim)
                .filter(StringUtils::isNotBlank)
                .map(Integer::parseInt)
                .filter(num -> num <= 1000)
                .collect(Collectors.toList());
        List<Integer> negatives = numbers.stream()
                .filter(number -> number < 0)
                .collect(Collectors.toList());
        if(!negatives.isEmpty()){
            throw new NegativeNumberExeption(negatives);
        }

        return numbers.stream()
                .reduce(Integer::sum)
                .orElse(0); // to uruchamia się zawsze
//                .orElseGet(() -> supperLongAndProccessorConsumingMethod); // to uruchamia się wtedy,
//                                                           kiedy jest potrzeba, bo to jest lambda


    }
}
