package pl.sda.intermediate.other;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter

public class CarOption {
    private String itemName;
    private BigDecimal price;

    public CarOption(String itemName, Integer price) {
        this.itemName = itemName;
        this.price = new BigDecimal(price);
    }
}
