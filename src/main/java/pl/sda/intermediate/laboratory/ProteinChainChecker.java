package pl.sda.intermediate.laboratory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ProteinChainChecker {
    ProteinChainDAO proteinChainDAO = ProteinChainDAO.getInstance();

    public List<Boolean> getAnswer() {
        List<String> chainList = proteinChainDAO.getChainList();
        List<Boolean> answers = new ArrayList<>();
        for (int i = 0; i < chainList.size(); i += 2) {
            Boolean answer = changePossible(chainList.get(i), chainList.get(i + 1));
            answers.add(answer);
        }
        return answers;
    }



    public static Boolean changePossible(String chain1, String chain2) {

        if (chain1.length() != chain2.length()) {
            return false;
        }
        if (chain1.equals(chain2)){
            return true;
        }

        String sortedChain1 = Arrays.stream(chain1.split(""))
                .sorted()
                .collect(Collectors.joining());
        String sortedChain2 = Arrays.stream(chain2.split(""))
                .sorted()
                .collect(Collectors.joining());
        return sortedChain1.equals(sortedChain2);
    }
}
