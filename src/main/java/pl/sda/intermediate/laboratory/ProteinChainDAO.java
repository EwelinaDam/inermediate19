package pl.sda.intermediate.laboratory;

import lombok.Getter;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class ProteinChainDAO {
    private static ProteinChainDAO instance;
    @Getter
    private List<String> chainList;

    //prywatny konstruktor
    private ProteinChainDAO() {
        chainList = createListOfChainsFromFile();
    }


    //metoda getInstance
    public static ProteinChainDAO getInstance() {
        if (instance == null) {
            synchronized (ProteinChainDAO.class) {
                if (instance == null) {
                    instance = new ProteinChainDAO();
                }
            }
        }
        return instance;
    }

    //metoda czytająca linie z pliku, tworząca litę łańcuchów białkowych
    private List<String> createListOfChainsFromFile() {
        ClassLoader classLoader = this.getClass().getClassLoader();
        try {
            Path path = Paths.get(classLoader.getResource("lancuchy_bialkowe_dane.txt").toURI());
            return Files.readAllLines(path);
        } catch (IOException | URISyntaxException e) {
            return new ArrayList<>();
        }
    }

}


