package pl.sda.intermediate.books;

import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class BooksDAO {




    //metoda, która weźmie elementy listy od 256 do 161244 i wytnie z listy puste linie
    private List<String> cutUslessData() {
        return createListofLines()
                .subList(257, 161244).stream()
                .filter(str -> StringUtils.isNotBlank(str))
                .collect(Collectors.toList());
    }

    //metoda czytająca linie z pliku, tworząca litę
    private List<String> createListofLines() {
        ClassLoader classLoader = this.getClass().getClassLoader();
        try {
            Path path = Paths.get(classLoader.getResource("GUTINDEX.txt").toURI());
            return Files.readAllLines(path);
        } catch (IOException | URISyntaxException e) {
            return new ArrayList<>();
        }
    }

}
