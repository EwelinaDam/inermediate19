package pl.sda.intermediate.books;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Map;

@Getter
@AllArgsConstructor
public class Books {
    private Integer id;
    private String name;

}

