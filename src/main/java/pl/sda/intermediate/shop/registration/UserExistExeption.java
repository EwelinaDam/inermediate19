package pl.sda.intermediate.shop.registration;

public class UserExistExeption extends RuntimeException {
    public UserExistExeption(String email) {
        super("User with email: "+ email + " already exists.");
    }
}
