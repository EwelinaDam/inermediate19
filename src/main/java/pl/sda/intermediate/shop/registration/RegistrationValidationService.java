package pl.sda.intermediate.shop.registration;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

public class RegistrationValidationService {

    public Map<String, String> validateUserData(RegistrationDTO registrationDTO) {
        Map<String, String> errorsMap = new HashMap<>();
        if (registrationDTO.getFirstName() == null ||
                !registrationDTO.getFirstName().matches("^[A-Z][a-z]{2,}$")) {
            errorsMap.put("firstNameValRes",
                    "Imię jest wymagane. Przynajmniej 3 znaki oraz pierwsza duża, reszta małe.");
        }
        if (registrationDTO.getLastName() == null ||
                !registrationDTO.getLastName().matches("^[A-Z][a-z]{2,}(-[A-Z][a-z]{2,})?$")) {
            errorsMap.put("lastNameValRes",
                    "Nazwisko jest wymagane." +
                            " Przynajmniej 3 znaki oraz pierwsza duża," +
                            " reszta małe \n + dopuszczenie \"-\" i drugiego członu z dużej litery");
        }
        if (registrationDTO.getZipCode() == null ||
                !registrationDTO.getZipCode().matches("^[0-9]{2}-[0-9]{3}$")) {
            errorsMap.put("zipCodeValRes",
                    "Zły format. Kod pocztowy powinien mieć format 12-345");
        }
        if (StringUtils.isBlank(registrationDTO.getCountry())) {
            errorsMap.put("countryValRes",
                    "Podanie nazwy kraju jest wymagane");
        }
        if (StringUtils.isBlank(registrationDTO.getCity())) {
            errorsMap.put("cityValRes",
                    "Podanie nazwy miasta jest wymagane");
        }
        if (StringUtils.isBlank(registrationDTO.getStreet())) {
            errorsMap.put("streetValRes",
                    "Podanie nazwy ulicy jest wymagane");
        }
        if (registrationDTO.getBirthDate() == null ||
                !registrationDTO.getBirthDate()
                        .matches("^(19|20)[0-9]{2}-(0[1-9]|1[0-2])-(0[1-9]|[12][0-9]|3[0-1])$")) {
            errorsMap.put("birthdayValRes",
                    "Zły format. Data urodzin powinna być podana w formacie RRRR-MM-DD");
        }
        if (registrationDTO.getPesel() == null ||
                !registrationDTO.getPesel()
                        .matches("^[0-9]{11}$")) {
            errorsMap.put("peselValRes",
                    "Zły format. Numer PESEL powinien składać się z 11 cyfr.");
        }
        if (registrationDTO.getEMail() == null ||
                !registrationDTO.getEMail()
                        .matches("^([\\w.]{3,})@([A-Za-z0-9]{2,})(\\.([a-z]{2,})){1,2}$")) {
            errorsMap.put("eMailValRes",
                    "Zły format adresu email");
        }
        if (registrationDTO.getPassword() == null ||
                !registrationDTO.getPassword()
                        .matches("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z]).{10,20}$")) {
            errorsMap.put("passwordValRes",
                    "Hasło jest wymagane. Musi zawierać od 10 do 20 znaków." +
                            " Minimum jedna duża, jedna mała litera i jedna cyfra.");
        }
        if (registrationDTO.getPhone() == null ||
                !registrationDTO.getPhone()
                        .matches("^(\\+[0-9]{1,3} )?(([0-9]{3})(-?)){2}([0-9]{3})$")) {
            errorsMap.put("phoneValRes",
                    "Zły format. Numer telefonu powinien składać się z 9 cyfr." +
                            " Możliwa opcja z \"+48\" na początku");
        }

        return errorsMap;
    }

}
