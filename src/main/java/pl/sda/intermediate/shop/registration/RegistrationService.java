package pl.sda.intermediate.shop.registration;

import org.apache.commons.codec.digest.DigestUtils;

public class RegistrationService {
    private UserDAO userDAO;

    public RegistrationService(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    public void register (RegistrationDTO registrationDTO){
        if(userDAO.checkIfEmailExist(registrationDTO.getEMail())){
            throw new UserExistExeption(registrationDTO.getEMail());
        }

        User user = createUser(registrationDTO);

        userDAO.addNewUser(user);
    }

    private User createUser(RegistrationDTO registrationDTO) {
        User user = new User();

        user.setFirstName(registrationDTO.getFirstName());
        user.setLastName(registrationDTO.getLastName());
        user.setBirthdate(registrationDTO.getBirthDate());
        user.setEMail(registrationDTO.getEMail());
        user.setPesel(registrationDTO.getPesel());
        user.setPhone(registrationDTO.getPhone());
        user.setPreferEmails(registrationDTO.getPreferEmails());
        user.setPasswordHash(DigestUtils.sha512Hex(registrationDTO.getPassword()));

        UserAddress address = new UserAddress();
        address.setCity(registrationDTO.getCity());
        address.setCountry(registrationDTO.getCountry());
        address.setStreet(registrationDTO.getStreet());
        address.setZipCode(registrationDTO.getZipCode());
        user.setUserAddress(address);
        return user;
    }
}
