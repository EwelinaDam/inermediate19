package pl.sda.intermediate.shop.registration;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

//userDAO ma odpowiedzialność: tylko przechowywać dane
public class UserDAO {
    public static final String FILEPATH = "C:/Users/Piotrek i Ewelina/listofusers.txt";
    Map <String,User> mapOfUsersByMail = readFromFile();

    void addNewUser(User user) {
        mapOfUsersByMail.put(user.getEMail(),user);

        try (FileOutputStream fos = new FileOutputStream(FILEPATH); //wpisz całą ścieżkę
             ObjectOutputStream oos = new ObjectOutputStream(fos)) {
            oos.writeObject(mapOfUsersByMail);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Map<String,User> readFromFile() {
        try (FileInputStream fis = new FileInputStream(FILEPATH); //wpisz całą ścieżkę
             ObjectInputStream ois = new ObjectInputStream(fis)) {
            Object o = ois.readObject();
            if( o instanceof List){
                List<User> user = (List<User>) o;
                Map<String, User> userMap = user.stream().collect(Collectors.toMap(u -> u.getEMail(), u -> u));
                return userMap;
            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return new HashMap<>();
    }

    public Optional<User> findUserByEmail(String email) {
        return Optional.ofNullable(mapOfUsersByMail.get(email));

    }

    boolean checkIfEmailExist(String userEmail) {
        return mapOfUsersByMail.containsKey(userEmail);
    }
}
