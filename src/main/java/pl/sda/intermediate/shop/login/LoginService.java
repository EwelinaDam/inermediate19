package pl.sda.intermediate.shop.login;

import org.apache.commons.codec.digest.DigestUtils;
import pl.sda.intermediate.shop.registration.User;
import pl.sda.intermediate.shop.registration.UserDAO;

import java.util.Optional;

public class LoginService {
    private UserDAO userDAO;

    public LoginService(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    public boolean login(LoginDTO loginDTO) {
        boolean ableToLog = userDAO
                .findUserByEmail(loginDTO.getLogin())
                .map(user ->
                        compareHashes(loginDTO, user))
                .orElse(false);
        if(ableToLog){
            UserContextHolder.addLoggedUser(loginDTO);
        }
        return ableToLog;
    }

    private boolean compareHashes(LoginDTO loginDTO, User user) {
        return user.getPasswordHash().equals(DigestUtils.sha512Hex(loginDTO.getPassword()));
    }

}
