package pl.sda.intermediate.shop.login;

import org.springframework.stereotype.Service;

@Service
public class UserContextHolder {
    private static String login; //loginem jest email usera

    public static void addLoggedUser(LoginDTO loginDTO) {
        login = loginDTO.getLogin();
    }

    public static String getUserLoggedIn() {
        return login;
    }

    public static void logOut() {
        login = null;
    }
}
