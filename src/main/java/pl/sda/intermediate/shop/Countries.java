package pl.sda.intermediate.shop;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
public enum Countries {
    POLAND("PL" ,"Polska"), ENGLAND("ENG", "Anglia"),
    GERMANY("DE","Niemcy"), SPAIN("ES","Hiszpania");

    private String symbol;
    private String polishName;
}
