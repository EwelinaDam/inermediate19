package pl.sda.intermediate.shop.categories;

import lombok.Getter;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class CategoryDAO { //Data Access Object
    private static CategoryDAO instance;
    @Getter
    private List<Category> categoryList;

    private CategoryDAO() {
        categoryList = initializeCategories();
    }

    public static CategoryDAO getInstance() {
        if (instance == null) {
            synchronized (CategoryDAO.class) {
                if (instance == null) {
                    instance = new CategoryDAO();
                }
            }
        }
        return instance;
    }

    private List<Category> initializeCategories() {
        List<String> lines = readLinesFromFile();
        AtomicInteger counter = new AtomicInteger(1); //odporny na wielowątkowość
        List<Category> list = lines.stream()
                .map(l -> populateCategory(l, counter)).collect(Collectors.toList());

        Map<Integer, List<Category>> groupByLevel =
                list.stream().collect(Collectors.groupingBy(i -> i.getLevel()));

        populateParentId(groupByLevel, 0);
//        return list; i już
        return groupByLevel.values()
                .stream()
                .flatMap(e -> e.stream())
                .collect(Collectors.toList());

    }

    private void populateParentId(Map<Integer, List<Category>> groupByLevel, int level) {
        if (!groupByLevel.containsKey(level)){
            return;
        }
        List<Category> children = groupByLevel.get(level);
        for (Category child : children){
            if (level == 0){
                continue;
            }
                List<Category> potentialParents = groupByLevel.get(level - 1);
                Integer parentId = potentialParents.stream()
                        .map(p -> p.getId())
                        .filter(id -> id < child.getId())
                        .sorted(Comparator.reverseOrder())
                        .findFirst()
                        .orElse(0);// jeżeli optional będzie pusty, to zwróci podaną przez nas wartość
            child.setParentId(parentId);
            }
            populateParentId(groupByLevel, level + 1); // rekurencja, aby znaleźć parentów dla wszystkich elementów

        }


    private Category populateCategory(String line, AtomicInteger counter) {
        Category category = new Category();
        category.setLevel(calculateLevel(line));
        category.setCategName(line.trim());
        category.setId(counter.getAndIncrement());
        return category;
    }

    private Integer calculateLevel(String categoryName) {
        if (categoryName.startsWith(" ")) {
            String[] split = categoryName.split("\\S+");
            return split[0].length();
        } else {
            return 0;
        }
    }

    private List<String> readLinesFromFile() {
        ClassLoader classLoader = this.getClass().getClassLoader();
        try {
            Path path = Paths.get(classLoader.getResource("categories.txt").toURI());
            return Files.readAllLines(path);
        } catch (IOException | URISyntaxException e) {
            return new ArrayList<>();
        }
    }
}
