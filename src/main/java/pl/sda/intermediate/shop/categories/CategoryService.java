package pl.sda.intermediate.shop.categories;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CategoryService {

    public List<CategoryDTO> findCategories(String searchTekst) {
        CategoryDAO categoryDAO = CategoryDAO.getInstance();
        Map<Integer, CategoryDTO> dtosMap = categoryDAO.getCategoryList().stream()
                .map(e -> convertToCategoryDTO(e)).collect(Collectors.toMap(e -> e.getId(), e -> e));
        dtosMap.values().stream()
                .filter(e -> e.getName().equals(StringUtils.defaultIfBlank(searchTekst, "").trim()))
                .forEach(foundCategory -> {
                    foundCategory.markAsSelected();
                    openParent(foundCategory, dtosMap);
                });
        return new ArrayList<>(dtosMap.values());
    }

    private void openParent(CategoryDTO categoryDTO, Map<Integer, CategoryDTO> dtosMap) {
        Integer parentId = categoryDTO.getParentId();
        if (parentId == null) {
            return;
        }
        CategoryDTO parent = dtosMap.get(parentId);
        parent.markAsOpen();
        openParent(parent, dtosMap);
    }

    private CategoryDTO convertToCategoryDTO(Category category) {
        CategoryDTO categoryDTO = new CategoryDTO();
        categoryDTO.setId(category.getId());
        categoryDTO.setParentId(category.getParentId());
        categoryDTO.setName(category.getCategName());
        categoryDTO.setState(new CategoryState(false, false));
        return categoryDTO;
    }
}
