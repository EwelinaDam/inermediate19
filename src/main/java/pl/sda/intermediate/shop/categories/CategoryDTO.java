package pl.sda.intermediate.shop.categories;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CategoryDTO { // zestaw danych potrzebnych na froncie do wyświetlenia drzewa kategorii to jest wysyłąne do frontendu, musi być dostosowane do frontu
    private Integer id;
    private Integer parentId;
    private String name;
    CategoryState state; // tak wymaga od nas biblioteka JS tree

    public String getParent(){
        return parentId == null ? "#" : String.valueOf(parentId);
    }

    public String getText(){
        return name;
    }


    public void markAsOpen(){
        state.setOpened(true);
    }
    public void markAsSelected (){
        state.setSelected(true);
    }


}
