package pl.sda.intermediate.shop;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.sda.intermediate.shop.categories.CategoryDTO;
import pl.sda.intermediate.shop.categories.CategoryService;
import pl.sda.intermediate.shop.login.LoginDTO;
import pl.sda.intermediate.shop.login.LoginService;
import pl.sda.intermediate.shop.login.UserContextHolder;
import pl.sda.intermediate.shop.registration.*;
import pl.sda.intermediate.shop.weather.WeatherService;
import pl.sda.intermediate.shop.weather.WeatherWraper;

import java.util.List;
import java.util.Map;

@Controller // to zbiera requesty ze strony www
public class OnlyOneController {
    private CategoryService categoryService = new CategoryService();
    private RegistrationValidationService validationService = new RegistrationValidationService();
    private UserDAO userDAO = new UserDAO();
    private RegistrationService registrationService = new RegistrationService(userDAO);
    private LoginService loginService = new LoginService(userDAO);
    private WeatherService weatherService = new WeatherService(userDAO);


    @RequestMapping("/categories")
    public String categories(@RequestParam(required = false) String input, Model model) {
        List<CategoryDTO> categories = categoryService.findCategories(input);
        model.addAttribute("catsdata", categories);
        return "categoriesPage"; //nazwa pliku html
    }

    //to wyświetla pusty formularz
    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String showForm(Model model) {
        model.addAttribute("form", new RegistrationDTO());
        model.addAttribute("countries", Countries.values());
        return "registerPage"; //tutaj ma się wyświtlać ta strona (registerPage.com)

    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String showLoginForm(Model model) {
        model.addAttribute("form", new LoginDTO());
        return "loginPage";
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logout(Model model) {
        UserContextHolder.logOut();
        model.addAttribute("form", new LoginDTO());
        return "loginPage";
    }

    @ResponseBody //wysyła JSON, do odpowiedniego miejsca w JVS
    @RequestMapping(value = "/weather", method = RequestMethod.GET)
    public WeatherWraper weather() {
        WeatherWraper weatherWraper = weatherService.downloadWeather();
        return weatherWraper;
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String loggedIn(LoginDTO loginDTO, Model model) {
        if (loginService.login(loginDTO)) {
            return "index";
        } else {
            model.addAttribute("error", "Błąd logowania");
            model.addAttribute("form", loginDTO);
            return "loginPage";
        }
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String registration(@ModelAttribute RegistrationDTO registrationDTO, Model model) {
        Map<String, String> mapOfErrors = validationService.validateUserData(registrationDTO);

        model.addAttribute("form", registrationDTO);
        model.addAttribute("countries", Countries.values());

        if (mapOfErrors.isEmpty()) {
            //rejestrujemy
            try {
                registrationService.register(registrationDTO);
            } catch (UserExistExeption e) {
                model.addAttribute("userExistsExeption", e.getMessage());
                return "registerPage";
            }

            return "helloPage";
        } else {
            model.addAllAttributes(mapOfErrors); // tutaj mergujemy dwie mapy, bo model to też mapa
            return "registerPage"; //fixme
        }
    }
}
