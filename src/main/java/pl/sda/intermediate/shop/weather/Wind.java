
package pl.sda.intermediate.shop.weather;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Wind {

    @SerializedName("speed") //to są opcjonalne rzeczy, te adnotacje
    @Expose
    public Double speed;
    @SerializedName("deg")
    @Expose
    public Integer deg;

}
