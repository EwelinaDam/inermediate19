package pl.sda.intermediate.shop.weather;

import retrofit2.http.GET;
import retrofit2.http.Query;

import java.util.concurrent.CompletableFuture;

public interface WeatherMethods {
    @GET ("data/2.5/weather")
    CompletableFuture <WeatherWraper> byCity (
            @Query("q") String city, //miasto dla którego ma być podana pogoda
            @Query("appid") String apiKey,
            @Query("units") String unit, //jednostka, żeby w metrach, a nie w sążniach
            @Query("lang") String language); //żeby było np. w języku polskim

}
