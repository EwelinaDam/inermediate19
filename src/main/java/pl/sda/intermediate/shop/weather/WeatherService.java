package pl.sda.intermediate.shop.weather;

import com.google.gson.Gson;
import pl.sda.intermediate.shop.login.UserContextHolder;
import pl.sda.intermediate.shop.registration.UserDAO;
import retrofit2.Retrofit;
import retrofit2.adapter.java8.Java8CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import java.util.concurrent.CompletableFuture;

public class WeatherService {
    private UserDAO userDAO;
    private String baseUrl = "https://openweathermap.org";
    private String apiKey = "ea900b66f547fd7b23625544873a4200";

    public WeatherService(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    public WeatherWraper downloadWeather (){
        String userLoggedIn = UserContextHolder.getUserLoggedIn();
        String userCity = userDAO.findUserByEmail(userLoggedIn)
                .map(u -> u.getUserAddress().getCity())
                .orElseThrow(() -> new RuntimeException("City Not Found"));


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(Java8CallAdapterFactory.create()) //dzięki temu możemy użyć completable future
                .build();

        WeatherMethods weatherMethods = retrofit.create(WeatherMethods.class);
        CompletableFuture<WeatherWraper> cf //nowy wątek osługujący pogodę, element opakowujący ta pracę w dodatkowy wątek
                = weatherMethods.byCity(userCity, apiKey, "metric", "pl");

        WeatherWraper join = cf.join();
        return join;

    }
}
