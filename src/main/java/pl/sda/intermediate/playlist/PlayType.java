package pl.sda.intermediate.playlist;

import lombok.Getter;

@Getter
public enum PlayType {
    RANDOM("losowo"), //to są już gotowe instancje enuma, to jest też singleton
    LOOP("w pętli"),
    SEQUENCE ("po kolei");

    private String plName;

    PlayType (String plName){
        this.plName = plName;
    }

}
