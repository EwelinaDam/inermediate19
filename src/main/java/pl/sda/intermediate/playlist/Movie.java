package pl.sda.intermediate.playlist;

public class Movie extends Playable {
    private String name;

    public Movie(String name) {
        this.name = name;
    }

    @Override
    public String play() {
        return "Film: " + name;
    }
}
