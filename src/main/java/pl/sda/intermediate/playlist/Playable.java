package pl.sda.intermediate.playlist;

public abstract class Playable {
    public abstract String play ();
}
