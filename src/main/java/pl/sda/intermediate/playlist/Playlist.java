package pl.sda.intermediate.playlist;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Playlist extends Playable {
    private PlayType playType = PlayType.SEQUENCE;
    private List<Playable> media = new ArrayList<>();


    public void addToPlaylist(Playable playable) {
        media.add(playable);
    }

    public void choosePlayType(PlayType playType) {
        this.playType = playType;
    }

    @Override
    public String play() {
        switch (playType) {
            case SEQUENCE:
                return playSequence(media);
            case LOOP:
                return IntStream.rangeClosed(0, 10)
                        .mapToObj(i -> playSequence(media))
                        .collect(Collectors.joining("\n ---\n"));
            case RANDOM:
//        List<Playable> copy = media; to jest tylko zmienna, która dalej odwołuje się do danych wejściowych
                // nie chcemy modyfikować danych wejściowych dlatego tworzymy kopię
                List<Playable> copy = new ArrayList<>(media); //musimy utworzyć nowy obiekt
                Collections.shuffle(copy); //miesza elementy na liście
                return playSequence(copy);
            default:
                return "Nie wybrałeś sposobu odtwarzania";
        }
    }

    private String playSequence(List<Playable> list) {
        return list.stream()
                .map(m -> m.play())
                .collect(Collectors.joining("\n"));
    }
}
